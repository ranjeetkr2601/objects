let isObject = require('./isObject.cjs');
function mapObject(obj, mapFunction){
    if(typeof mapFunction === 'function' || isObject(obj)){
        for(let key in obj){
            obj[key] = mapFunction(obj[key]);
        }
    }
}
module.exports = mapObject;