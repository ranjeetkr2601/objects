let keys = require('./keys.cjs');
function pairs(obj) {
    let key = keys(obj);
    let length = key.length;
    let pairs = [];
    if(typeof key !== 'undefiend' && key.length > 0){
        for (let index = 0; index < length; index++) {
            pairs.push([key[index], obj[key[index]]]);
        }
    }
    return pairs;
  }
  module.exports = pairs;