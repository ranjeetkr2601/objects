let isObject = require('./isObject.cjs');
function keys(obj){
    
    let keys = [];
    if(isObject(obj)){
        for (let key in obj){
            keys.push(key);
        }
    }
    return keys;
}
module.exports = keys;

