let isObject = require('./isObject.cjs');

function defaults(obj, defaultObj){
    const length = defaults.length;
    if(isObject(obj) && isObject(defaultObj)){
        for(let defaultKey in defaultObj){
            if(!(defaultKey in obj) || (typeof obj[defaultKey] === 'undefined')){
                obj[defaultKey] = defaultObj[defaultKey];
            }
        }
    }
    return obj;
}
module.exports = defaults;