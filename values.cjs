let keys = require('./keys.cjs')
function values(obj){
    let key = keys(obj);
    let length = key.length;
    let values = [];
    if(typeof key !== 'undefiend' && key.length > 0){
        for(let index = 0; index < length; index++){
            if(obj.hasOwnProperty(key[index])){
                values.push(obj[key[index]]);
            }
        }
    }
    return values;
}
module.exports = values;