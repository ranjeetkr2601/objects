let keys = require('./keys.cjs');

function invert(obj){
    let key = keys(obj);
    let invertedObject = {};
    for(let index = 0; index < key.length; index++){
        invertedObject[obj[key[index]]] = key[index];
    }
    return invertedObject;
}
module.exports = invert;